var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var jsmin = require('gulp-jsmin');
var cleanCSS = require('gulp-clean-css');


//Styles
gulp.task('sass', function () {
  return gulp.src('styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css'));
});

//Scripts
gulp.task('scripts', function() {
  return gulp.src('scripts/**/*.js')
    .pipe(concat('all.js'))
    .pipe(jsmin())
    .pipe(gulp.dest('dist/js'));
});

//Watch
gulp.task('watch', function(){
    gulp.watch('styles/**/*.scss', ['sass']);
    gulp.watch('scripts/**/*.js', ['scripts']);
});
